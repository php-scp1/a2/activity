<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S2: Repetition Control Structures and Array Manipulation</title>
</head>
<body>
	<h1>Repetition Control Structures</h1>

	<h2>While Loop</h2>
	<?php whileLoop(); ?>

	<h2>Do-While Loop</h2>
	<?php doWhileLoop(); ?>

	<h2>For Loop</h2>
	<?php forLoop(); ?>

	<h2>Continue and Break</h2>
	<?php modifiedForLoop(); ?>

	<h1>Array Manipulation</h1>

	<h2>Types of Arrays:</h2>

	<h3>Simple Array</h3>
	<ul>
		<?php foreach($grades as $grade){ ?>
			<li><?= $grade; ?></li>
		<?php } ?>
	</ul>

	<h3>Associative Array</h3>
	<ul>
		<?php foreach($gradePeriods as $period => $grade){ ?>
			<li>Grade in <?= $period . " is " . $grade; ?></li>
		<?php } ?>
	</ul>

	<h3>Multi-Dimensional Array</h3>
	<ul>
		<?php
			foreach($heroes as $team){
				foreach($team as $member){
				?>
					<li><?= $member; ?></li>
				<?php }
			}
		?>
	</ul>

	<h3>Multi-Dimensional Associative Array</h3>
	<ul>
		<?php
			foreach($powers as $label => $powerGroup){
				foreach($powerGroup as $power){
				?>
					<li><?= "$label: $power"; ?></li>
				<?php }
			}
		?>
	</ul>

	<h1>Array Functions</h1>

	<h3>Add to Array</h3>

	<?php array_push($computerBrands, 'Apple'); ?>
	<pre><?php print_r($computerBrands); ?></pre>

	<?php array_unshift($computerBrands, 'Dell'); ?>
	<pre><?php print_r($computerBrands); ?></pre>

	<h3>Remove from Array</h3>

	<?php array_pop($computerBrands); ?>
	<pre><?php print_r($computerBrands); ?></pre>

	<?php array_shift($computerBrands); ?>
	<pre><?php print_r($computerBrands); ?></pre>

	<h3>Count</h3>
	<p><?= count($computerBrands); ?></p>

	<h3>In Array</h3>
	<p><?= searchBrand('HP', $computerBrands); ?></p>
</body>
</html>
