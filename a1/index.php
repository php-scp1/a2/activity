<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S2: A2</title>
</head>
<body>
	<h3>Divisible of Five</h3>
	<?php printDivisibleByFive(); ?>

	<h3>Array Manipulation</h3>
	<?php $students = []; ?>
	<?php array_push($students, "John Smith"); ?>
	<p><?php var_dump($students); ?></p>
	<p><?php count($students); ?></p>

	<?php array_push($students, "Jane Smith"); ?>
	<p><?php var_dump($students); ?></p>
	<p><?php count($students); ?></p>

	<?php array_shift($students); ?>
	<p><?php var_dump($students); ?></p>
	<p><?php count($students); ?></p>

</body>
</html>
